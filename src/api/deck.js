import axios from 'axios'


export const getCards = () => {
    return axios.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
        .then(res => {
            return res.data
        })
}

export const drawTwoCards = (deckID) => {
    return axios.get(`https://deckofcardsapi.com/api/deck/${deckID}/draw/?count=2`)
        .then(res => {
            return res.data
        })
}

export const drawOneCard = (deckID) => {
    return axios.get(`https://deckofcardsapi.com/api/deck/${deckID}/draw/?count=1`)
        .then(res => {
            return res.data
        })
}