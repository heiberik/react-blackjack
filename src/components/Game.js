import React, { useEffect, useState } from 'react'
import { drawTwoCards, drawOneCard } from '../api/deck'

import './Game.css'

const Game = ({ deckID, newGameClicked }) => {

    const [dealerCards, setDealerCards] = useState([])
    const [playerCards, setPlayerCards] = useState([])
    const [dealerPoints, setDealerPoints] = useState(0)
    const [playerPoints, setPlayerPoints] = useState(0)
    const [winner, setWinner] = useState(null)
    const [pass, setPass] = useState(false)
    const [showPass, setShowPass] = useState(true)
    const [showDraw, setShowDraw] = useState(true)


    useEffect(() => {

        let dPoints = 0
        dealerCards.forEach(e => {
            let sum = 0
            if (e.value === "JACK") sum = 10
            else if (e.value === "QUEEN") sum = 10
            else if (e.value === "KING") sum = 10
            else if (e.value === "ACE") {
                if (dPoints + 11 <= 21) sum = 11
                else sum = 1
            }
            else sum = parseInt(e.value)
            dPoints += sum
        })

        setDealerPoints(dPoints)

        let pPoints = 0
        playerCards.forEach(e => {
            let sum = 0
            if (e.value === "JACK") sum = 10
            else if (e.value === "QUEEN") sum = 10
            else if (e.value === "KING") sum = 10
            else if (e.value === "ACE") {
                if (pPoints + 11 <= 21) sum = 11
                else sum = 1
            }
            else sum = parseInt(e.value)
            pPoints += sum
        })

        setPlayerPoints(pPoints)

        checkWinner(pPoints, dPoints, false)

    }, [playerCards, dealerCards])


    useEffect(() => {
        if (pass) {
            if ((dealerPoints < 15 && dealerPoints < playerPoints) || (dealerPoints >= 16 && dealerPoints <= 20 && playerPoints <= 21 && playerPoints > dealerPoints)) {
                drawOneCard(deckID)
                    .then(res => {
                        setTimeout(() => {
                            setDealerCards(dc => dc.concat(res.cards[0]))
                        }, 2000)
                    })
            }
            else if (pass) checkWinner(playerPoints, dealerPoints, true)
        }
        else {
            checkWinner(playerPoints, dealerPoints, false)
        }
    }, [playerPoints, dealerPoints, pass, deckID])


    useEffect(() => {
        if (deckID) {
            drawTwoCards(deckID)
                .then(res => {
                    setDealerCards(res.cards)
                })
            drawTwoCards(deckID)
                .then(res => {
                    setPlayerCards(res.cards)
                })
        }
    }, [deckID])


    const checkWinner = (pPoints, dPoints, done) => {

        let winnerName = null

        if (done) {
            if (dPoints > 21) winnerName = "PLAYER"
            else if (dPoints === pPoints) winnerName = "DRAW"
            else if (dPoints >= pPoints) winnerName = "DEALER"
            else winnerName = "PLAYER"
        }
        else {
            if (pPoints > 21 || dPoints > 21) {
                if (pPoints < dPoints) winnerName = "PLAYER"
                else if (pPoints === dPoints) winnerName = "DRAW"
                else winnerName = "DEALER"
            }
        }

        if (winnerName) {
            setTimeout(() => {
                setWinner(winnerName)
            }, 1000)
        }
    }

    const drawCardHandler = () => {

        drawOneCard(deckID)
            .then(res =>
                setPlayerCards(playerCards.concat(res.cards[0]))
            )

        dealerLogic()
    }

    const dealerLogic = () => {


        if (playerPoints > 21) {
            checkWinner(playerPoints, dealerPoints, true)
            return
        }
        if ((dealerPoints < 15 && dealerPoints < playerPoints) || (dealerPoints >= 16 && dealerPoints <= 20 && playerPoints <= 21 && playerPoints > dealerPoints)) {
            drawOneCard(deckID)
                .then(res => {
                    setTimeout(() => {
                        setDealerCards(dc => dc.concat(res.cards[0]))
                    }, 2000)
                })
        }
        else if (pass) checkWinner(playerPoints, dealerPoints, true)
    }


    const passHandler = async () => {
        setPass(true)
        setShowPass(false)
        setShowDraw(false)
    }

    if (!winner) {
        return (
            <div id="gameContainer">

                <div id="choiceContainer">
                    {showDraw && <button className="choiceButton" onClick={drawCardHandler}> DRAW CARD </button>}
                    {showPass && <button className="choiceButton" onClick={passHandler}> PASS </button>}
                </div>

                <div className="cardList">
                    <h1 className="headerText"> Dealer Cards </h1>
                    <div className="cards">
                        {dealerCards.map(c => { return <img key={c.image} alt="hehe" className="cardImg" src={c.image} /> })}
                    </div>
                    <h1 className="headerPoints"> {dealerPoints} </h1>
                </div>
                <div className="cardList">
                    <h1 className="headerText"> Player Cards </h1>
                    <div className="cards">
                        {playerCards.map(c => { return <img key={c.image} alt="hehe" className="cardImg" src={c.image} /> })}
                    </div>
                    <h1 className="headerPoints"> {playerPoints} </h1>
                </div>

                <div id="choiceContainer">
                   
                </div>
            </div>
        )
    }
    else return (
        <div id="gameContainer">

            <div id="choiceContainer">
                {winner === "DRAW" ? <h1 className="winnerText"> GAME OVER - IT'S A DRAW {winner} </h1> :
                    <h1 className="winnerText"> GAME OVER - WINNER IS {winner} </h1>}
            </div>
            <div className="cardList">
                <h1 className="headerText"> Dealer Cards </h1>
                <div className="cards">
                    {dealerCards.map(c => { return <img key={c.image} alt="hehe" className="cardImg" src={c.image} /> })}
                </div>
                <h1 className="headerPoints"> {dealerPoints} </h1>
            </div>

            <div className="cardList">
                <h1 className="headerText"> Player Cards </h1>
                <div className="cards">
                    {playerCards.map(c => { return <img key={c.image} alt="hehe" className="cardImg" src={c.image} /> })}
                </div>
                <h1 className="headerPoints"> {playerPoints} </h1>
            </div>

            <div id="choiceContainer">
                <button className="choiceButton" onClick={newGameClicked} > NEW ROUND </button>
            </div>
        </div>
    )
}

export default Game