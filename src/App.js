import React, { useEffect, useState } from 'react'
import './App.css'
import { getCards } from './api/deck'
import Game from './components/Game'

const App = () => {


	const [gameStarted, setGameStarted] = useState(false)
	const [deckID, setDeckID] = useState(null)
	const [deck, setDeck] = useState(null)

	useEffect(() => {
		if (gameStarted){
			getCards()
			.then(res => {
				setDeckID(res.deck_id)
				setDeck(res)
				console.log(res)
			})
		}
	}, [gameStarted])

	const startGameHandler = () => {
		setGameStarted(true)
	}

	const newGameClicked = () => {

	}


	if (!gameStarted){
		return (
			<div className="App">
				<button id="startGameButton" onClick={startGameHandler}> START GAME </button>
			</div>
		)
	}
	else return (
		<div className="App">
			<Game deckID={deckID} newGameClicked={newGameClicked} /> 
		</div>
	)
}

export default App
